""" RevuesDePresse Class """

import os
import time
import calendar

from interfaces import IRevuesDePresse
from zope.interface import implements
from Products.Archetypes.public import *


schema = BaseSchema + Schema((
    TextField('description',
              widget=RichWidget(description_msgid="desc_folder",
                                    description="The description of the presse reviews folder.",
                                    label_msgid="label_folder",
                                    label="Description",
                                    i18n_domain = "revuesdepresse",
                                    rows=15)),
    StringField('pressreviewsbaseurl',
                 widget=StringWidget(description="Base URL of press reviews",
                                      description_msgid="desc_pressreviewsbaseurl",
                                      label_msgid="label_pressreviewsbaseurl",
                                      label="Press Reviews Base URL",
                                      i18n_domain = "revuesdepresse"),
                 required=1,
                 searchable=0),
    StringField('pressreviewspath',
                 widget=StringWidget(description="Path to server directory with press reviews",
                                      description_msgid="desc_pressreviewspath",
                                      label_msgid="label_pressreviewspath",
                                      label="Press Reviews Path",
                                      i18n_domain = "revuesdepresse"),
                 required=1,
                 searchable=0),
    ))

class RevuesDePresse(BaseContent):

    implements(IRevuesDePresse)

    schema = schema
    _at_rename_after_creation = True

    def getTodayLink(self):
        if not self.pressreviewsbaseurl[-1] == '/':
            self.pressreviewsbaseurl += '/'
        return self.pressreviewsbaseurl + 'revue.pdf'

    def getLinks(self, num=None, offset=0):
        '''Return a list of tuples (url, label, (y,m,d))'''
        if not self.pressreviewsbaseurl[-1] == '/':
            self.pressreviewsbaseurl += '/'
        filenames = os.listdir(self.pressreviewspath)
        def cmp_filenames(x, y):
            if len(x) != 18:
                return -1
            if len(y) != 18:
                return 1
            return cmp((y[10:14], y[8:10], y[6:8]), (x[10:14], x[8:10], x[6:8]))
        filenames.sort(cmp_filenames)
        all_links = []
        for filename in filenames:
            if not filename.startswith('Review'):
                continue
            if not os.path.splitext(filename)[-1].lower() == '.pdf':
                continue
            url = self.pressreviewsbaseurl + filename
            y, m, d = filename[10:14], filename[8:10], filename[6:8]
            label = '%s/%s/%s' % (d, m, y)
            all_links.append((url, label, (int(y), int(m), int(d))))
        if num is None:
            return all_links[offset:]
        return all_links[offset:offset+num]

    def getRangeOfYears(self):
        first_year = self.getLinks()[-1][2][0]
        return range(int(first_year), time.localtime()[0]+1)

    def getYear(self, year):
        if not year:
            year = time.localtime()[0]
        return int(year)

    def getArchives(self, year):
        if not year:
            year = time.localtime()[0]
        links_dict = {}
        for url, label, date in self.getLinks():
            links_dict[date] = url
        t = []
        for month in range(12):
            caltuples = calendar.monthcalendar(int(year), month+1)
            t.append('<table class="ploneCalendar">')
            t.append('<thead>')
            t.append('<tr><th colspan="7">%s / %s</th></tr>' % (month+1, year))
            t.append('</thead>')
            t.append('<tbody>')
            for weektuple in caltuples:
                t.append('<tr>')
                for day in weektuple:
                    t.append('<td>')
                    if day:
                        date = (int(year), int(month+1), int(day))
                        if date in links_dict:
                            t.append('<a href="%s">%s</a>' % (links_dict[date], day))
                        else:
                            t.append('%s' % day)
                    t.append('</td>')
                t.append('</tr>')
            t.append('</tbody>')
            t.append('</table>')
            if month % 3 == 2:
                t.append('<br />')
        return '\n'.join(t)

registerType(RevuesDePresse)
