"""Interfaces for RevuesDePresse"""

from zope.interface import Interface

class IRevuesDePresse(Interface):
    """A folder that contains Press Reviews"""

